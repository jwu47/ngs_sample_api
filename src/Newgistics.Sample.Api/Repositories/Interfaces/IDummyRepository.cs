﻿using Newgistics.Lib.ServiceHost.Dto;
using System.Threading.Tasks;

namespace Newgistics.SampleApi.Service.Repositories.Interfaces
{
    public interface IDummyRepository
    {
        Task<BooleanResult> GetDummy(string id);
    }
}
