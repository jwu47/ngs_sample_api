#!/usr/bin/env groovy
import groovy.json.JsonSlurper

// deploys all images that have subfolders in the ./helm folder.
// provides a dropdown in jenkins with env choices, those choices coincide with namespaces in the cluster.

podTemplate(
    label: 'dnc-sdk-2',
    containers: [
        containerTemplate(
            name: 'jnlp',
            image: 'ngsnonprod.azurecr.io/aci_k8s/jenkins-slave-dnc-sdk:2.0',
            ttyEnabled: true,
            command: 'jenkins-slave',
            privileged: false,
            alwaysPullImage: true,
            workingDir: '/home/jenkins',
            args: '${computer.jnlpmac} ${computer.name}')
        ],
    volumes: [
        secretVolume(secretName: 'registrylogin', mountPath: '/registrylogin'),
        hostPathVolume(mountPath: '/usr/bin/docker', hostPath: '/usr/bin/docker'),
        hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock'),
        hostPathVolume(mountPath: '/root/.kube', hostPath: '/root/.kube')
    ]
)

{
    node('dnc-sdk-2') {
        try {
            REPO_PROJ = 'mic_dnc'
            MJR_MIN_VER = '1.0'
            SECRET_VOL = 'registrylogin'
            SLACK_CHANNEL = '#microservice-ci'

            if (JENKINS_URL.contains(".azint.")) {
                DEPLOY_ENV = (params.DEPLOY_ENV != null) ? params.DEPLOY_ENV : 'int'
                REPO_LOCATION = 'ngsnonprod'
            } else {
                DEPLOY_ENV = (params.DEPLOY_ENV != null) ? params.DEPLOY_ENV : 'prod'
                REPO_LOCATION = 'ngsprod'
            }

            stage('checkout') {
                // slackSend channel: "${SLACK_CHANNEL}", color: 'good', message: "started ${env.JOB_NAME} ${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open>)"
                checkout scm
                dir('charts') {
                    git branch: 'master', credentialsId: 'svc_git_teamcity', url: 'https://svc_git_teamcity@bitbucket.org/ngsteam/ngs_aci_k8s_helm-charts.git'
                }
            }

            stage('deploy') {
                sh "az login --service-principal -u `cat /registrylogin/azuser` -p `cat /registrylogin/azpassword` --tenant edc7c787-c840-48ea-bbb2-078bf7e11100"
                sh "helm init --client-only"
                DOCKER_USERNAME = sh(script: "cat /${SECRET_VOL}/username", returnStdout: true) 
                DOCKER_PASSWORD = sh(script: "cat /${SECRET_VOL}/password", returnStdout: true) 
                HELM_DIRS = sh(script: "ls ./helm", returnStdout: true).tokenize() 
                
                for (i =0; i < HELM_DIRS.size(); i++) {
                    DEPLOY_NAME = HELM_DIRS[i]
                    DEPLOY_TYPE = DEPLOY_NAME.tokenize('-').reverse()[0]
                
                    stdout = sh(script: "az acr repository show-tags -n ${REPO_LOCATION} --repository ${REPO_PROJ}/${DEPLOY_NAME} -u ${DOCKER_USERNAME} -p ${DOCKER_PASSWORD}", returnStdout: true)
                    def tags = new JsonSlurper().parseText(stdout)
                    sortedTags = sortListNewestFirst(tags)
                    SELECTED_TAG = sortedTags[0]  //set default to highest version number.  This will be the selection if nothing else is chosen within one minute.
                    try {
                            timeout(time:59, unit:'SECONDS') {
                            SELECTED_TAG = input(
                                id: 'IMAGE_TAG', message: "Deploying '$DEPLOY_NAME':", parameters: [
                                    [$class: 'ChoiceParameterDefinition', choices: sortedTags.join("\n"), description: "If no selection is made, version $SELECTED_TAG will be deployed in less than a minute.", name: 'Choose Version:']
                                ]
                            )
                        }
                    } catch (err) {
                        def user = err.getCauses()[0].getUser().toString().replaceAll("\\s","").toUpperCase()
                        if (!user.equals("SYSTEM")) {
                            echo "Aborted by:\n ${user}"
                            throw err  //rethrow because it was really aborted not a time out.
                        }
                    }
                    sh "helm upgrade --install ${DEPLOY_ENV}-${DEPLOY_NAME} --namespace=${DEPLOY_ENV} -f ./helm/${DEPLOY_NAME}/values-${DEPLOY_ENV}.yaml  --set image.tag=${SELECTED_TAG} ./charts/stable/dnc-${DEPLOY_TYPE}"
                    // slackSend channel: "${SLACK_CHANNEL}", color: 'good', message: "${DEPLOY_NAME}:${SELECTED_TAG} deployed to ${DEPLOY_ENV}"
                }
            }

            if (DEPLOY_ENV == 'int') {
                stage('test') {
                    try {
                        sh """export ASPNETCORE_ENVIRONMENT=integration
                            cd \$WORKSPACE/test
                            for TEST_DIR in *Integration ; do
                                cd \$TEST_DIR
                                dotnet restore
                                dotnet test
                                cd ../
                            done
                            cd ../
                            URL=`cat /${SECRET_VOL}/url`
                            USERNAME=`cat /${SECRET_VOL}/username`
                            PASSWORD=`cat /${SECRET_VOL}/password`
                            docker login \$URL -u \$USERNAME -p \$PASSWORD
                            cd ./helm
                            for REPO_NAME in * ; do
                                TAG_VNEXT="${REPO_LOCATION}.azurecr.io/${REPO_PROJ}/\$REPO_NAME:v-next"
                                TAG_INT="${REPO_LOCATION}.azurecr.io/${REPO_PROJ}/\$REPO_NAME:integration"
                                docker pull  \$TAG_VNEXT
                                docker tag \$TAG_VNEXT \$TAG_INT
                                docker push \$TAG_INT
                            done
                        """
                    } catch (err) {
                        bitbucketStatusNotify( buildState: 'FAILURE', buildDescription: 'unit tests failed.' )
                        throw err //rethrow exception after notifying bitbucket.
                    }
                }
            }
        }
        catch (any) {
            // slackSend channel: "${SLACK_CHANNEL}", color: 'danger', message: "deployment failed ${env.JOB_NAME} ${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open>)"
            currentBuild.result = 'FAILURE'
            throw any //rethrow exception to prevent the build from proceeding
        }
        finally {
            step([$class: 'Mailer', notifyEveryUnstableBuild: true, recipients: 'jwester@newgistics.com', sendToIndividuals: true])
        }
    }
}

@NonCPS
def sortListNewestFirst(List listToSort) {
    return listToSort.sort(false) {x, y ->
    def xa = x.tokenize('.'); def ya = y.tokenize('.')
    def sz = Math.min(xa.size(), ya.size())
    for (int i = 0; i < sz; i++) {
        def xs = xa[i]; def ys = ya[i];
        if (xs.isInteger() && ys.isInteger()) {
        def xn = xs.toInteger()
        def yn = ys.toInteger()
        if (xn != yn) { return xn <=> yn }
        }// else if (xs != ys) {
        //  return xs <=> ys
    // }
    // uncommenting this will make the sort more true alphanumeric, but I want the non-number tags to be at the back of the reverse sort.
    }
    return xa.size() <=> ya.size()
    }.reverse()
}
