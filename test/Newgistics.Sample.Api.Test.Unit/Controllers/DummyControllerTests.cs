﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Newgistics.Lib.ServiceHost.Dto;
using Newgistics.Lib.ServiceHost.Helper;
using Newgistics.SampleApi.Service.Controllers;
using Newgistics.SampleApi.Service.Repositories.Interfaces;
using Shouldly;
using Xunit;

namespace Newgistics.SampleApi.Service.Test.Unit.Controllers
{
    public class DummyControllerTests
    {
        [Fact]
        public async void GetByBarcodeRetunsSuccess()
        {
            //Arrange
            var mockController = new Mock<DummyController>() { CallBase = true };
            
            //Setup dependencies
          
            //Act
            var result =  mockController.Object.GetReource("dummy");

            //Assert
            result.ShouldNotBeNull();
            var okResult = result.ShouldBeAssignableTo<OkObjectResult>();
            var rest = okResult.Value.ShouldBeAssignableTo<RestOkResponse<object>>();
            
        }

    }

}
