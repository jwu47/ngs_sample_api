﻿using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Net.Http;
using System;
using System.Data;
using System.IO;
using Microsoft.Extensions.Configuration;
using Xunit;
using System.Data.SqlClient;
using System.Threading;

namespace Newgistics.SampleApi.Service.Test.Integration
{
    //ref: https://xunit.github.io/docs/shared-context.html#collection-fixture
    public class BaseTestFixture : IDisposable
    {
        internal string apiBaseUri = "";
        internal string httpMediaType = "application/json";
        public IConfiguration config;

        public BaseTestFixture()
        {
            var testSettings = GetTestSettings();

            config = testSettings;
            apiBaseUri = testSettings.GetSection("ServiceEndpoints")["Service"];

          //  BuildDb();
        }

        public void Dispose()
        {
            // ... clean up test data from the database ...
        }

        public void Initialize()
        {
        }

        internal async Task<HttpResponseMessage> GetAsync(string httpMediaType, string requestUri)
        {
            using (var client = BuildClient(requestUri, null, httpMediaType))
            {
                return await client.GetAsync(requestUri);
            }
        }

        private static HttpClient BuildClient(string baseUri, Dictionary<string, string> headers = null, string acceptEncoding = "application/json")
        {
            var client = new HttpClient {BaseAddress = new Uri(baseUri)};
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(acceptEncoding));

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }

            return client;
        }

        private IConfiguration GetTestSettings()
        {
            var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            System.Console.Write("env: " + environmentName);

            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("testsettings.json", false, true)
                .AddJsonFile($"testsettings.{environmentName}.json", true, true)
                .AddEnvironmentVariables();

            var configuration = builder.Build();
            return configuration;
        }

    }

    [CollectionDefinition("BaseTestFixture Collection")]

    public class BaseTestFixtureCollection : ICollectionFixture<BaseTestFixture>
    {
        // This class has no code, and is never created. Its purpose is simply
        // to be the place to apply [CollectionDefinition] and all the
        // ICollectionFixture<> interfaces.
    }
}
