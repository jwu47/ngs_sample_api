﻿using Newgistics.Lib.ServiceHost.Helper;
using Newgistics.Lib.ServiceHost.Dto;
using System.Threading.Tasks;
using System.Reflection;
using System.Net.Http;
using System.Net;
using Xunit.Sdk;
using Xunit;

namespace Newgistics.SampleApi.Service.Test.Integration.Test
{
    [Collection("BaseTestFixture Collection")]
    public class DummyControllerTests_Int
    {
        private static readonly string _serviceController = "v1/resource/{id}";
        static string _apiEndpointUrl = "";
        static MethodInfo _methodUnderTest;
        private readonly BaseTestFixture fixture;

        public DummyControllerTests_Int(BaseTestFixture fixture)
        {
            this.fixture = fixture;
            Assert.True(!string.IsNullOrWhiteSpace(fixture.apiBaseUri));
            _apiEndpointUrl = string.Format("{0}{1}", fixture.apiBaseUri, _serviceController);
            System.Console.WriteLine("===" + _apiEndpointUrl);
        }


        private class DisplayTestMethodNameAttribute : BeforeAfterTestAttribute
        {

            public override void Before(MethodInfo methodUnderTest)
            {
                _methodUnderTest = methodUnderTest;
            }

            public override void After(MethodInfo methodUnderTest)
            {
                _methodUnderTest = methodUnderTest;
            }
        }

        #region DummyController endpoints
       
        [Fact]
        [DisplayTestMethodName]
        [Trait("Category", "ControllerAccess-Label")]
        public async Task Get_WithoutId_ShouldBeNotFound()
        {
            var apiHttpResponseMessage =
                await fixture.GetAsync(fixture.httpMediaType, _apiEndpointUrl.Replace("{id}", string.Empty));
            Assert.Equal("HttpStatusCode: " + HttpStatusCode.NotFound, "HttpStatusCode: " + apiHttpResponseMessage.StatusCode);
        }


        [Theory]
        [InlineData("123", "123")]
        [DisplayTestMethodName]
        [Trait("Category", "ControllerAccess-Label")]
        public async Task Get_WithId_ShouldBeSuccess(string id, string expectedResult)
        {
            var urlString = _apiEndpointUrl.Replace("{id}", id);
            System.Console.WriteLine("..." + urlString);
            var apiHttpResponseMessage =
                await fixture.GetAsync(fixture.httpMediaType, urlString);
            Assert.Equal("HttpStatusCode: " + HttpStatusCode.OK, "HttpStatusCode: " + apiHttpResponseMessage.StatusCode);


            var booleanResult = await apiHttpResponseMessage.Content.ReadAsAsync<RestOkResponse<BooleanResult>>();
            Assert.True(booleanResult.Data.Success);
            Assert.True(booleanResult.Data.Message == expectedResult);
        }

     
        #endregion
    }
}
